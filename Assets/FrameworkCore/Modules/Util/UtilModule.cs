﻿/*
 *  date: 2018-04-21
 *  author: John-chen
 *  cn: 工具模块
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 工具模块
    /// </summary>
    public class UtilModule : BaseModule
    {
        public UtilModule(string name = ModuleName.Util):base(name)
        {

        }

        public override void InitGame()
        {
            PathHelper.CreateSingleton();
            MonoHelper.CreateMonoHelperRoot();
        }
    }
}
