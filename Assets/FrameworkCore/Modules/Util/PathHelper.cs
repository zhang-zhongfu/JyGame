﻿/*
 *  date: 2018-06-29
 *  author: John-chen
 *  cn: 路径管理
 *  en: todo:
 */

using System.IO;
using UnityEngine;

namespace JyFramework
{
    /// <summary>
    /// 路径管理
    /// </summary>
    public class PathHelper : Singleton<PathHelper>
    {
        /// <summary>
        /// assetbundle 的后缀名(jyassetbundle) todo:暂时不设置
        /// </summary>
        public static string AssetBundleVariant { get { return "jyassetbundle"; } }

        /// <summary>
        /// 资源文件夹
        /// </summary>
        public static string ResPath { get { return Application.dataPath + "/ResourcesCore/"; } }

        /// <summary>
        /// 代码文件夹
        /// </summary>
        public static string CodePath { get { return Application.dataPath + "/GameCore/"; } }

        /// <summary>
        /// 场景文件夹
        /// </summary>
        public static string ScenePath { get { return ResPath + "_scenes/"; } }

        /// <summary>
        /// UI文件夹
        /// </summary>
        public static string UIPath { get { return "resourcescore/_ui/"; } }

        /// <summary>
        /// UIRoot路径
        /// </summary>
        public static string UIRootPath { get { return "resourcescore/_ui/uiroot.prefab.jyassetbundle"; } }

        /// <summary>
        /// assetbundle 路径
        /// 1. editor 下,AssetBundle 路径是 StreamingAssets
        /// 2. 非 editor 下，AssetBunlde 路径是
        ///     Android:    Application.persistentDataPath
        ///     ios:        "jar:file://" + Application.persistentDataPath
        /// </summary>
        public string AssetBundlePath { get { return _assetBundlePath; } }

        /// <summary>
        /// 通过文件路径获取文件名
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string GetFileName(string path)
        {
            if (string.IsNullOrEmpty(path)) return "";

            string name = Path.GetFileName(path);
            // 没有后缀
            bool isVariant = string.IsNullOrEmpty(AssetBundleVariant);
            if (isVariant) return name;

            // 有后缀
            string fullName = name;
            int cnt = fullName.IndexOf("." + AssetBundleVariant);
            if (cnt > -1)
                name = fullName.Substring(0, cnt);

            return name;
        }

        /// <summary>
        /// 获取UI的文件路径
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GetUIPath(string name)
        {
            return UIPath + name;
        }

        /// <summary>
        /// 构造初始化
        /// </summary>
        protected override void Init()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.WindowsEditor:
                    _assetBundlePath = Application.streamingAssetsPath + "/AssetBundle/";
                    break;
                case RuntimePlatform.IPhonePlayer:
                    _assetBundlePath = Application.persistentDataPath + "/Raw/AssetBundle/";
                    break;
                case RuntimePlatform.Android:
                    _assetBundlePath = "jar:file://" + Application.persistentDataPath + "!/assets//AssetBundle/";
                    break;
            }
        }


        /// <summary>
        /// assetbundle 路径
        /// </summary>
        private string _assetBundlePath;
    }
}