﻿/*
 *  date: 2018-06-29
 *  author: John-chen
 *  cn: 日志管理器
 *  en: todo:
 */

using JyFramework;
using UnityEngine;

namespace GameCore.BaseCore
{
    /// <summary>
    /// date: 2018-06-29
    /// author: John-Chen
    /// 日志管理器
    /// </summary>
    public class LogMgr : Singleton<LogMgr>
    {
        /// <summary>
        /// info级输出
        /// </summary>
        /// <param name="content"></param>
        public static void Info(object content)
        {
            Ins._log.Info(content);
        }

        /// <summary>
        /// warning级输出
        /// </summary>
        /// <param name="content"></param>
        public static void Warning(object content)
        {
            Ins._log.Warning(content);
        }

        /// <summary>
        /// error级输出
        /// </summary>
        /// <param name="content"></param>
        public static void Error(object content)
        {
            Ins._log.Error(content);
        }

        /// <summary>
        /// 是否在editor上显示
        /// </summary>
        public static bool IsShowEditor
        {
            get { return Ins._log.IsShowEditor; }
            set { Ins._log.IsShowEditor = value; }
        }

        /// <summary>
        /// 是否存储到本地
        /// </summary>
        public static bool IsSaveToClient
        {
            get { return Ins._log.IsSaveToClient; }
            set { Ins._log.IsSaveToClient = value; }
        }

        /// <summary>
        /// 是否发送到服务器
        /// </summary>
        public static bool IsSendToServer
        {
            get { return Ins._log.IsSendToServer; }
            set { Ins._log.IsSendToServer = value; }
        }

        /// <summary>
        /// 日志打印类
        /// </summary>
        /// <param name="content"></param>
        /// <param name="lv"></param>
        private void LogPrint(object content, LogLevel lv)
        {
            switch (lv)
            {
                case LogLevel.Info:
                    Debug.Log(content);
                    break;
                case LogLevel.Warning:
                    Debug.LogWarning(content);
                    break;
                case LogLevel.Error:
                    Debug.LogError(content);
                    break;
            }
        }

        /// <summary>
        /// 初始化
        /// </summary>
        protected override void Init()
        {
            _log = JyApp.Ins.GetModule<LogModule>(ModuleName.Log).Log;
            _log.SetLogDelegate(LogPrint);
        }

        private JyLog _log;
    }
}